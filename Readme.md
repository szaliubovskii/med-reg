##### Buzzwords:
- .NET Core 2.0
- Proto.Actor
- RethinkDb
- Autofac
- Automapper
- xUnit

##### External dependencies:
###### RethinkDb
Management UI: http://localhost:8080
```shell
docker run --name registration-rethink -d -p 12010:8080 -p 28015:28015 -p 29015:29015 -d rethinkdb
```
###### Consul
Management UI: http://localhost:8500
```shell
docker run --name=registration-consul -p 8500:8500 -p 8600:8600 -p 8300-8302:8300-8302 -p 4000:4100 -p 5000:5100 -d consul agent -ui -client 0.0.0.0 -dev

```

###### RavenDB
```
docker run -p 8080:8080 -d --name registration-ravendb -e UNSECURED_ACCESS_ALLOWED=PublicNetwork ravendb/ravendb
```