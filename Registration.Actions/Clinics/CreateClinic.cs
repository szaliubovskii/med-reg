﻿namespace Registration.Actions.Clinics
{
    public class CreateClinic : BaseAction, IClinicAction
    {
        public string ClinicName { get; set; }
    }
}