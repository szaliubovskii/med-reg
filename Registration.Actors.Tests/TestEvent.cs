﻿using System;
using Registration.Events;

namespace Registration.Actors.Tests
{
    public class TestEvent : IEvent
    {
        public TestEvent(long val)
        {
            Id = Guid.NewGuid().ToString();
            Test = val;
        }

        public long Test { get; }

        #region Implementation of IEvent

        public string Id { get; }

        #endregion
    }
}