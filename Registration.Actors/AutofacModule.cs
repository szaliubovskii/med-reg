﻿using System.Runtime.CompilerServices;
using Autofac;
using Registration.Actors.Clinics;
using Registration.Actors.Common;
using Registration.Actors.Implementation;
using Registration.Actors.Interfaces;

[assembly: InternalsVisibleTo("Registration.Actors.Tests")]

namespace Registration.Actors
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ErrorHandler>().AsSelf();
            builder.RegisterType<ClinicCommandHander>().AsSelf();
            builder.RegisterType<ClinicRegistrationCommandHander>().AsSelf();
            builder.RegisterGeneric(typeof(EventSourcedState<,>)).As(typeof(IEventSourcedState<,>));
            builder.RegisterType<Clinic>().AsSelf();
            builder.RegisterType<ClinicRegistry>().AsSelf();
            builder.RegisterType<ClinicRegistryProjection>().AsSelf();
            builder.RegisterType<ClinicClusterNode>().AsSelf();
        }
    }

    public class ClientAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClinicRegistrationDispatcher>().AsSelf();
        }
    }
}