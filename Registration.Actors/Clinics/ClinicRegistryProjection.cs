﻿using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Registration.Data.Projections;
using Registration.Events.Clinics.ClinicRegistration;
using Registration.Journals.Events;

namespace Registration.Actors.Clinics
{
    public class ClinicRegistryProjection
    {
        private readonly EventJournalFactory _journalFactory;
        private readonly ClinicRegistrationProjection _projection;

        public ClinicRegistryProjection(EventJournalFactory journalFactory, ClinicRegistrationProjection projection)
        {
            _journalFactory = journalFactory;
            _projection = projection;
        }

        public async Task Start()
        {
            await _projection.EnsureStorageAsync();
            var journalName = typeof(IClinicRegistrationEvent).Name;
            var journal = await _journalFactory(journalName).EnsureJournalAsync();
            await journal.Listen("ClinicRegistration", 0).SubscribeOn(TaskPoolScheduler.Default).Do(async entry =>
            {
                await _projection.Project(entry);
            });
        }
    }
}