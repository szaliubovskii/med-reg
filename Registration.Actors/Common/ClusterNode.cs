﻿using Google.Protobuf;
using MessagePack.Resolvers;
using Microsoft.Extensions.Logging;
using Proto;
using Proto.Cluster;
using Proto.Cluster.Consul;
using Proto.Remote;

namespace Registration.Actors.Common
{
    internal class MessagePackSerializer : ISerializer
    {
        public MessagePackSerializer()
        {
            MessagePack.MessagePackSerializer.SetDefaultResolver(ContractlessStandardResolver.Instance);
        }

        #region Implementation of ISerializer

        public ByteString Serialize(object obj)
        {
            var data = MessagePack.MessagePackSerializer.Serialize(obj);
            return ByteString.CopyFrom(data, 0, data.Length);
        }

        public object Deserialize(ByteString bytes, string typeName)
        {
            return MessagePack.MessagePackSerializer.Deserialize<object>(bytes.ToByteArray());
        }

        public string GetTypeName(object message)
        {
            return message.GetType().Name;
        }

        #endregion
    }

    public abstract class ClusterNode
    {
        private readonly ILoggerFactory _loggerFactory;

        protected ClusterNode(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory;
        }

        public void Start(string clusterName, int port)
        {
            Log.SetLoggerFactory(_loggerFactory);
            Serialization.RegisterSerializer(new MessagePackSerializer(), true);
            RegisterKinds();
            Cluster.Start(clusterName, "127.0.0.1", port, new ConsulProvider(new ConsulProviderOptions()));
        }

        protected abstract void RegisterKinds();

        public abstract void StartProjection();
    }
}