﻿using System.Threading.Tasks;
using Registration.Data.Models;
using Registration.Events.Clinics.ClinicRegistration;
using Registration.Journals.Events;
using Registration.RethinkDb.Common;
using RethinkDb.Driver;
using RethinkDb.Driver.Ast;
using RethinkDb.Driver.Net;

namespace Registration.Data.Projections
{
    public class ClinicRegistrationProjection
    {
        private static readonly RethinkDB R = RethinkDB.R;
        private readonly IRethinkConnectionProvider _connectionProvider;

        public ClinicRegistrationProjection(IRethinkConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        private string TableName => Constants.ClinicRegistryTable;

        private Table Table => R.Table(TableName);

        private Connection Connection => _connectionProvider.Connection;

        public Task EnsureStorageAsync()
        {
            return Connection.EnsureTableAsync(Connection.Db, TableName);
        }

        public async Task Project(IEventJournalEntry entry)
        {
            switch (entry.Event)
            {
                case ClinicRegistered registered:
                    await Table.Insert(new ClinicDef
                    {
                        Id = registered.Id,
                        Name = registered.ClinicName,
                        Address = registered.ClinicAddress
                    }).RunAsync(Connection);
                    break;
            }
        }
    }
}