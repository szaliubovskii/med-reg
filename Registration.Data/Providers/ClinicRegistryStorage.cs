﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Registration.Data.Models;
using Registration.RethinkDb.Common;
using RethinkDb.Driver;
using RethinkDb.Driver.Ast;
using RethinkDb.Driver.Net;

namespace Registration.Data.Providers
{
    public class ClinicRegistryStorage
    {
        private static readonly RethinkDB R = RethinkDB.R;
        private readonly IRethinkConnectionProvider _connectionProvider;

        public ClinicRegistryStorage(IRethinkConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        private string TableName => Constants.ClinicRegistryTable;

        private Table Table => R.Table(TableName);

        private Connection Connection => _connectionProvider.Connection;

        public Task EnsureStorageAsync()
        {
            return Connection.EnsureTableAsync(Connection.Db, TableName);
        }

        public Task<IEnumerable<ClinicDef>> GetClinicsAsync()
        {
            return Table.RunResultAsync<IEnumerable<ClinicDef>>(Connection);
        }

        public IObservable<ClinicDef> Listen()
        {
            return Table
                .Changes()
                .OptArg("include_initial", true)
                .RunChanges<ClinicDef>(Connection)
                .ToObservable()
                .Select(c => c.NewValue);
        }
    }
}