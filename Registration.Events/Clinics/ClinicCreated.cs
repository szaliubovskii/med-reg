﻿namespace Registration.Events.Clinics
{
    public class ClinicCreated : BaseEvent, IClinicEvent
    {
        public string ClinicName { get; set; }

        public string ClinicAddress { get; set; }
    }
}