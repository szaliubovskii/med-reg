using Registration.Actions;

namespace Registration.Events
{
    public interface IActionMapper
    {
        TEvent Map<TEvent>(IAction action)
            where TEvent : class, IEvent;
    }
}