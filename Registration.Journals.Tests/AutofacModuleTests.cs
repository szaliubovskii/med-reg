﻿using System.Reflection;
using Autofac;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Registration.Journals.Errors;
using Registration.Journals.Events;
using Registration.RethinkDb.Common;
using Xbehave;
using Xunit;

namespace Registration.Journals.Tests
{
    [Collection(nameof(RethinkDbCollection))]
    public class AutofacModuleTests
    {
        private readonly RethinkDbFixture _fixture;

        private IContainer _container;

        public AutofacModuleTests(RethinkDbFixture fixture)
        {
            _fixture = fixture;
        }

        [Background]
        public void Background()
        {
            "Create container".x(
                () =>
                {
                    var dbName = Assembly.GetAssembly(typeof(RethinkDbFixture)).GetName().Name.Replace('.', '_');
                    var configuration = RethinkDbConfigurations.Default(dbName);
                    var builder = new ContainerBuilder();
                    builder.RegisterType<NullLoggerFactory>().As<ILoggerFactory>();
                    builder.Register(c => configuration).AsImplementedInterfaces();
                    builder.RegisterModule<RethinkDb.Common.AutofacModule>();
                    builder.RegisterModule<AutofacModule>();
                    _container = builder.Build();
                });
        }

        [Scenario]
        public void ErrorJournalResolving(ErrorJournalFactory factory, IErrorJournal journal)
        {
            "Resolve error journal factory".x(() => { factory = _container.Resolve<ErrorJournalFactory>(); });

            "Create error journal".x(() => { journal = factory(nameof(ErrorJournalResolving)); });

            "Feed should be created".x(() => { journal.Should().NotBeNull(); });
        }

        [Scenario]
        public void EventJournalResolving(IContainer container, EventJournalFactory factory, IEventJournal journal)
        {
            "Resolve event journal factory".x(() => { factory = _container.Resolve<EventJournalFactory>(); });

            "Create event journal".x(() => { journal = factory(nameof(EventJournalResolving)); });

            "Feed should be created".x(() => { journal.Should().NotBeNull(); });
        }
    }
}