﻿using System;
using System.Reflection;
using Microsoft.Extensions.Logging.Abstractions;
using Registration.RethinkDb.Common;
using Registration.RethinkDb.Common.Implementation;
using Xunit;

namespace Registration.Journals.Tests
{
    [CollectionDefinition(nameof(RethinkDbCollection))]
    public class RethinkDbCollection : ICollectionFixture<RethinkDbFixture>
    {
    }

    public class RethinkDbFixture : IDisposable
    {
        public RethinkDbFixture()
        {
            var dbName = Assembly.GetAssembly(typeof(RethinkDbFixture)).GetName().Name.Replace('.', '_');
            var configuration = RethinkDbConfigurations.Default(dbName);
            var connectionProvider = new ConnectionProvider(new NullLoggerFactory(), configuration);
            connectionProvider.Init();
            var conn = connectionProvider.Connection;
            conn.DeleteDatabaseAsync(conn.Db).Wait();
            conn.EnsureDatabaseAsync(conn.Db).Wait();
            ConnectionProvider = connectionProvider;
        }

        public IRethinkConnectionProvider ConnectionProvider { get; }

        public void Dispose()
        {
        }
    }
}