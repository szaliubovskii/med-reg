﻿using System;
using Registration.Events;

namespace Registration.Journals.Tests
{
    public class TestEvent : IEvent
    {
        public TestEvent()
        {
            Id = Guid.NewGuid().ToString();
        }

        #region Implementation of IEvent

        public string Id { get; set; }

        #endregion
    }
}