﻿using System;
using System.Threading.Tasks;

namespace Registration.Journals.Errors
{
    /// <summary>
    ///     Creates error journal with specified id
    /// </summary>
    /// <param name="journalName"></param>
    /// <returns></returns>
    public delegate IErrorJournal ErrorJournalFactory(string journalName);

    /// <summary>
    ///     Provides access to error journal
    /// </summary>
    public interface IErrorJournal
    {
        /// <summary>
        ///     Creates error journal in database if not exists
        /// </summary>
        /// <returns></returns>
        Task<IErrorJournal> EnsureJournalAsync();

        /// <summary>
        ///     Listens for errors occurred for specified correlation id
        /// </summary>
        /// <param name="trackId"></param>
        /// <returns>Returns <see cref="IActionErrorJournalEntry" /> or <see cref="IEventErrorJournalEntry" /></returns>
        IObservable<IErrorJournalEntry> Listen(string trackId);

        /// <summary>
        ///     Sends error for action to error journal
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        Task PublishActionAsync(IActionErrorJournalEntry entry);

        /// <summary>
        ///     Sends error for event to error journal
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        Task PublishEventAsync(IEventErrorJournalEntry entry);
    }
}