﻿using System;

namespace Registration.Journals.Errors
{
    /// <summary>
    ///     Contains information for error
    /// </summary>
    public interface IErrorJournalEntry : IJournalEntry
    {
        /// <summary>
        ///     Exception thrown
        /// </summary>
        Exception Exception { get; }
    }
}