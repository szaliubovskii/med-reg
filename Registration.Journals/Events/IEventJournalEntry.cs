﻿using Registration.Actions;
using Registration.Events;

namespace Registration.Journals.Events
{
    /// <summary>
    ///     Contains data for event stored in <see cref="IEventJournal" />
    /// </summary>
    public interface IEventJournalEntry : IJournalEntry
    {
        IAction Action { get; }
        ulong SequenceNumber { get; }

        /// <summary>
        ///     Payload with event data
        /// </summary>
        IEvent Event { get; }
    }
}