﻿using System;

namespace Registration.Journals
{
    public interface IJournalEntry
    {
        string Id { get; }

        string TrackId { get; }

        string AggregateKey { get; }

        /// <summary>
        ///     Timestamp when envelope was generated
        /// </summary>
        DateTime TimestampUtc { get; }
    }
}