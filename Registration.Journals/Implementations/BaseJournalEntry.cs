﻿using System;
using Newtonsoft.Json;

namespace Registration.Journals.Implementations
{
    internal abstract class BaseJournalEntry : IJournalEntry
    {
        #region Implementation of IJournalEntry

        [JsonProperty("id")]
        public string Id { get; set; }

        public string TrackId { get; set; }
        public string AggregateKey { get; set; }
        public DateTime TimestampUtc { get; set; }

        #endregion
    }

    public interface IJournalEntryBuilder<out TEntry, out TBuilder>
        where TEntry : IJournalEntry
        where TBuilder : IJournalEntryBuilder<TEntry, TBuilder>
    {
        TBuilder WithTrackId(string trackId);

        TBuilder WithAggregateKey(string aggregateKey);

        TEntry ToEntry();
    }

    internal abstract class
        BaseJournalEntryBuilder<TEntry, TEntryImp, TBuilder> : IJournalEntryBuilder<TEntry, TBuilder>
        where TEntry : IJournalEntry
        where TEntryImp : BaseJournalEntry, TEntry
        where TBuilder : IJournalEntryBuilder<TEntry, TBuilder>
    {
        protected BaseJournalEntryBuilder()
        {
            Entry = CreateEntry();
        }

        protected TEntryImp Entry { get; }

        protected abstract TBuilder This { get; }

        protected virtual void Initialize()
        {
            Entry.Id = GetId();
            Entry.TimestampUtc = DateTime.UtcNow;
        }

        protected abstract TEntryImp CreateEntry();

        protected abstract string GetId();

        #region Implementation of IJournalEntryBuilder<TEntry,out TBuilder>

        public TBuilder WithTrackId(string trackId)
        {
            Entry.TrackId = trackId;
            return This;
        }

        public TBuilder WithAggregateKey(string aggregateKey)
        {
            Entry.AggregateKey = aggregateKey;
            return This;
        }

        public TEntry ToEntry()
        {
            Initialize();
            return Entry;
        }

        #endregion
    }
}