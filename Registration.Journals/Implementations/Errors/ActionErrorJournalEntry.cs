using Registration.Actions;
using Registration.Journals.Errors;

namespace Registration.Journals.Implementations.Errors
{
    internal class ActionErrorJournalEntry : ErrorJournalEntry, IActionErrorJournalEntry
    {
        #region Implementation of IActionErrorJournalEntry

        public IAction Action { get; set; }

        #endregion
    }

    public interface
        IActionErrorJournalEntryBuilder : IErrorJournalEntryBuilder<IActionErrorJournalEntry,
            IActionErrorJournalEntryBuilder>
    {
        IActionErrorJournalEntryBuilder WithAction(IAction action);
    }

    internal class ActionErrorJournalEntryBuilder :
        ErrorJournalEntryBuilder<IActionErrorJournalEntry, ActionErrorJournalEntry, IActionErrorJournalEntryBuilder>,
        IActionErrorJournalEntryBuilder
    {
        #region Implementation of IActionErrorJournalEntryBuilder

        public IActionErrorJournalEntryBuilder WithAction(IAction action)
        {
            Entry.Action = action;
            return this;
        }

        #endregion

        #region Overrides of BaseJournalEntryBuilder<IActionErrorJournalEntry,ActionErrorJournalEntry,IActionErrorJournalEntryBuilder>

        protected override ActionErrorJournalEntry CreateEntry()
        {
            return new ActionErrorJournalEntry();
        }

        protected override string GetId()
        {
            return $"{Entry.AggregateKey}_{Entry.TrackId}_action_{Entry.Action.Id}";
        }

        protected override IActionErrorJournalEntryBuilder This => this;

        #endregion
    }
}