﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Registration.Journals.Events;
using Registration.RethinkDb.Common;

namespace Registration.Journals.Implementations.Events
{
    internal class EventJournal : BaseJournal, IEventJournal
    {
        public EventJournal(string journalName, IRethinkConnectionProvider connectionProvider) : base(journalName,
            "events", connectionProvider)
        {
        }

        private string AggregateIndex =>
            $"{nameof(IEventJournalEntry.AggregateKey)}_{nameof(IEventJournalEntry.SequenceNumber)}";

        #region Implementation of IEventJournal

        public async Task<IEventJournal> EnsureJournalAsync()
        {
            await EnsureJournalTableAsync();
            await Connection.EnsureCompoundIndexAsync(
                DbName,
                JournalTableName,
                AggregateIndex,
                new[] {nameof(IEventJournalEntry.AggregateKey), nameof(IEventJournalEntry.SequenceNumber)});
            return this;
        }

        public IObservable<IEventJournalEntry> Listen(string aggregateKey, ulong fromSequenceNumber)
        {
            return Table
                .Between(new object[] {aggregateKey, fromSequenceNumber}, new object[] {aggregateKey, R.Maxval()})
                .OptArg("index", AggregateIndex)
                .OptArg("left_bound", "closed") // >=
                .Changes().OptArg("include_initial", true).RunChanges<IEventJournalEntry>(Connection).ToObservable()
                .Select(c => c.NewValue);
        }

        public IObservable<IEventJournalEntry> Listen(string aggregateKey, string trackId)
        {
            return Table
                .Filter(g =>
                    g[nameof(IEventJournalEntry.AggregateKey)] == aggregateKey &&
                    g[nameof(IEventJournalEntry.TrackId)] == trackId)
                .Changes().OptArg("include_initial", true).RunChanges<IEventJournalEntry>(Connection).ToObservable()
                .Select(c => c.NewValue);
        }

        public async Task PublishAsync(IEventJournalEntry entry)
        {
            await Table
                .Insert(entry)
                .OptArg("durability", "hard")
                .OptArg("conflict", "error")
                .RunAsync(Connection);
        }

        public async Task<IEnumerable<IEventJournalEntry>> QueryAllAsync(string aggregateKey)
        {
            var result = await Table
                .Between(
                    new object[] {aggregateKey, R.Minval()},
                    new object[] {aggregateKey, R.Maxval()})
                .OptArg("index", AggregateIndex)
                .OptArg("left_bound", "open") // >
                .OrderBy().OptArg("index", AggregateIndex)
                .RunResultAsync<List<IEventJournalEntry>>(Connection);
            return result;
        }

        public async Task<IEnumerable<IEventJournalEntry>> QueryAsync(string aggregateKey, ulong fromSequenceNumber)
        {
            var result = await Table
                .Between(new object[] {aggregateKey, fromSequenceNumber}, new object[] {aggregateKey, R.Maxval()})
                .OptArg("index", AggregateIndex)
                .OptArg("left_bound", "closed") // >=
                .OrderBy().OptArg("index", AggregateIndex)
                .RunResultAsync<List<IEventJournalEntry>>(Connection);
            return result;
        }

        #endregion
    }
}