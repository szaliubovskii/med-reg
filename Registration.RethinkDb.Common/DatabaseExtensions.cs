﻿using System.Linq;
using System.Threading.Tasks;
using RethinkDb.Driver;
using RethinkDb.Driver.Net;

namespace Registration.RethinkDb.Common
{
    /// <summary>
    ///     Extension methods for RethinkDB connection help to manage rethink db in runtime
    /// </summary>
    public static class DatabaseExtensions
    {
        private static readonly RethinkDB R = RethinkDB.R;

        /// <summary>
        ///     Removes database
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static async Task DeleteDatabaseAsync(this Connection conn, string dbName)
        {
            var exists = await R.DbList().Contains(dbName).RunAtomAsync<bool>(conn);
            if (!exists) return;
            await R.Db(dbName).Wait_().RunAsync(conn);
            await R.DbDrop(dbName).RunAsync(conn);
        }

        /// <summary>
        ///     Removes table
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static async Task DeleteTableAsync(this Connection conn, string dbName, string tableName)
        {
            var exists = await R.Db(dbName).TableList().Contains(tableName).RunAtomAsync<bool>(conn);
            if (!exists) return;
            await R.Db(dbName).TableDrop(tableName).RunAsync(conn);
        }

        /// <summary>
        ///     Checks and creates compound index on several fields if not exists
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <param name="tableName"></param>
        /// <param name="indexName"></param>
        /// <param name="rows"></param>
        /// <returns></returns>
        public static async Task EnsureCompoundIndexAsync(
            this Connection conn,
            string dbName,
            string tableName,
            string indexName,
            string[] rows)
        {
            var exists = await R.Db(dbName).Table(tableName).IndexList().Contains(indexName).RunAtomAsync<bool>(conn);
            if (exists) return;
            await R.Db(dbName).Table(tableName).IndexCreate(indexName, expr => rows.Select(s => expr[s]).ToArray())
                .RunAsync(conn);
            await R.Db(dbName).Table(tableName).IndexWait(indexName).RunAsync(conn);
        }

        /// <summary>
        ///     Checks and creates database if not exists
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <returns></returns>
        public static async Task EnsureDatabaseAsync(this Connection conn, string dbName)
        {
            var exists = await R.DbList().Contains(dbName).RunAtomAsync<bool>(conn);
            if (exists) return;
            await R.DbCreate(dbName).RunAsync(conn);
            await R.Db(dbName).Wait_().RunAsync(conn);
        }

        /// <summary>
        ///     Checks and creates simple index if not exists
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <param name="tableName"></param>
        /// <param name="indexName"></param>
        /// <returns></returns>
        public static async Task EnsureIndexAsync(
            this Connection conn,
            string dbName,
            string tableName,
            string indexName)
        {
            var exists = await R.Db(dbName).Table(tableName).IndexList().Contains(indexName).RunAtomAsync<bool>(conn);
            if (exists) return;
            await R.Db(dbName).Table(tableName).IndexCreate(indexName).RunAsync(conn);
            await R.Db(dbName).Table(tableName).IndexWait(indexName).RunAsync(conn);
        }

        /// <summary>
        ///     Checks and creates table if not exists
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="dbName"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static async Task EnsureTableAsync(this Connection conn, string dbName, string tableName)
        {
            var exists = await R.Db(dbName).TableList().Contains(tableName).RunAtomAsync<bool>(conn);
            if (exists) return;
            await R.Db(dbName).TableCreate(tableName).RunAsync(conn);
            await R.Db(dbName).Table(tableName).Wait_().RunAsync(conn);
        }
    }
}