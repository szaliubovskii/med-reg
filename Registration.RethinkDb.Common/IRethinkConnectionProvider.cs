﻿using RethinkDb.Driver.Net;

namespace Registration.RethinkDb.Common
{
    public interface IRethinkConnectionProvider
    {
        Connection Connection { get; }
        void Init();
    }
}