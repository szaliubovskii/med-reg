﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Registration.Actions.Clinics.ClinicRegistration;
using Registration.Actors.Clinics;
using Registration.Data.Models;
using Registration.Data.Providers;

namespace Registration.Server.Hubs
{
    public class ClinicRegistrationHub : Hub
    {
        private readonly ClinicRegistrationDispatcher _dispatcher;
        private readonly ClinicRegistryStorage _storage;

        public ClinicRegistrationHub(ClinicRegistrationDispatcher dispatcher, ClinicRegistryStorage storage)
        {
            _dispatcher = dispatcher;
            _storage = storage;
        }

        public IObservable<ClinicDef> StreamClinics()
        {
            _storage.EnsureStorageAsync().Wait();
            return _storage.Listen().SubscribeOn(TaskPoolScheduler.Default);
        }

        public async Task AddClinic(string name)
        {
            var requestId = Guid.NewGuid().ToString();

            var result = await _dispatcher.TellAsync(requestId, new RegisterClinic
            {
                ClinicName = name,
                ClinicAddress = "Address-1"
            });

            if (result.Ex != null)
                throw result.Ex;
        }
    }
}