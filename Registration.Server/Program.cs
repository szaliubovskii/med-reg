﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Registration.Actors.Clinics;
using Registration.RethinkDb.Common;

namespace Registration.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            var dbName = "Registration";
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddConsole(LogLevel.Debug);
            var configuration = RethinkDbConfigurations.Default(dbName);
            builder.Register(c => loggerFactory).As<ILoggerFactory>();
            builder.Register(c => configuration).SingleInstance();
            builder.RegisterModule<AutofacModule>();
            builder.RegisterModule<Journals.AutofacModule>();
            builder.RegisterModule<Data.Projections.AutofacModule>();
            builder.RegisterModule<Actors.AutofacModule>();
            var container = builder.Build();

            var node = container.Resolve<ClinicClusterNode>();
            node.Start("Clinics", 12001);
            node.StartProjection();
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(services => services.AddAutofac())
                .UseStartup<Startup>()
                .UseUrls("http://localhost:5001/")
                .Build();
        }
    }
}