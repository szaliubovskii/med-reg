﻿using System;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Registration.Actions.Clinics.ClinicRegistration;
using Registration.Actors;
using Registration.Actors.Clinics;
using Registration.Data.Providers;
using Registration.RethinkDb.Common;
using Registration.Server.Hubs;
using AutofacModule = Registration.RethinkDb.Common.AutofacModule;

namespace Registration.Server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            var dbName = "Registration";
            var configuration = RethinkDbConfigurations.Default(dbName);
            builder.Register(c => configuration).SingleInstance();
            builder.RegisterModule<AutofacModule>();
            builder.RegisterModule<Journals.AutofacModule>();
            builder.RegisterModule<Data.Providers.AutofacModule>();
            builder.RegisterModule(new ClientAutofacModule());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(LogLevel.Debug);

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseFileServer();

            app.UseSignalR(routes => { routes.MapHub<ClinicRegistrationHub>("clinicRegistration"); });
        }

        private static void HandleRegistry(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                var dispatcher = context.RequestServices.GetService<ClinicRegistrationDispatcher>();
                var storage = context.RequestServices.GetService<ClinicRegistryStorage>();
                var requestId = Guid.NewGuid().ToString();
                var requestIdFeature = context.Features.Get<IHttpRequestIdentifierFeature>();
                if (requestIdFeature?.TraceIdentifier != null)
                    requestId = requestIdFeature.TraceIdentifier;
                var name = "Test-" + Guid.NewGuid().ToString();
                await dispatcher.TellAsync(requestId, new RegisterClinic
                {
                    ClinicName = name,
                    ClinicAddress = "Address-1"
                });

                await storage.EnsureStorageAsync();

                var subscription = storage.Listen()
                    .SubscribeOn(TaskPoolScheduler.Default);

                var clinics = await subscription
                    .TakeWhile(x => x.Name != name)
                    .ToList()
                    .ToTask();
                await context.Response.WriteAsync("<h1>qq</h1>");
                await context.Response.WriteAsync(string.Join("<br/>", clinics.Select(x => x.Name)));
            });
        }
    }
}